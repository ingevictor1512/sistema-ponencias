/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : ponencias_db

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 14/10/2021 22:01:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for areas
-- ----------------------------
DROP TABLE IF EXISTS `areas`;
CREATE TABLE `areas` (
  `idareas` int(11) NOT NULL,
  `areasnombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idareas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_key` varchar(45) NOT NULL,
  `config_value` varchar(100) NOT NULL,
  `config_desc` varchar(100) DEFAULT NULL,
  `config_type` int(255) NOT NULL,
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for documentos
-- ----------------------------
DROP TABLE IF EXISTS `documentos`;
CREATE TABLE `documentos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_real` varchar(80) NOT NULL,
  `nombre_mostrar` varchar(80) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `roles_aut` varchar(100) DEFAULT NULL,
  `type_documents` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for municipios
-- ----------------------------
DROP TABLE IF EXISTS `municipios`;
CREATE TABLE `municipios` (
  `idmunicipios` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `regiones_idregiones` int(11) NOT NULL,
  `cp` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idmunicipios`),
  KEY `fk_municipios_regiones_idx` (`regiones_idregiones`),
  CONSTRAINT `fk_municipios_regiones` FOREIGN KEY (`regiones_idregiones`) REFERENCES `regiones` (`idregiones`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ponencias
-- ----------------------------
DROP TABLE IF EXISTS `ponencias`;
CREATE TABLE `ponencias` (
  `idponencias` int(11) NOT NULL,
  `ponenciasnombre` varchar(45) DEFAULT NULL,
  `areas_idareas` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `regiones_idregiones` int(11) NOT NULL,
  PRIMARY KEY (`idponencias`),
  KEY `fk_ponencias_areas1_idx` (`areas_idareas`),
  KEY `fk_ponencias_usuario1_idx` (`usuario_id`),
  KEY `fk_ponencias_regiones1_idx` (`regiones_idregiones`),
  CONSTRAINT `fk_ponencias_areas1` FOREIGN KEY (`areas_idareas`) REFERENCES `areas` (`idareas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ponencias_regiones1` FOREIGN KEY (`regiones_idregiones`) REFERENCES `regiones` (`idregiones`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ponencias_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ponencias_documentos
-- ----------------------------
DROP TABLE IF EXISTS `ponencias_documentos`;
CREATE TABLE `ponencias_documentos` (
  `idponencias_documentos` int(11) NOT NULL AUTO_INCREMENT,
  `ponencias_idponencias` int(11) NOT NULL,
  `documentos_id` bigint(20) NOT NULL,
  PRIMARY KEY (`idponencias_documentos`),
  KEY `fk_ponencias_documentos_ponencias1_idx` (`ponencias_idponencias`),
  KEY `fk_ponencias_documentos_documentos1_idx` (`documentos_id`),
  CONSTRAINT `fk_ponencias_documentos_documentos1` FOREIGN KEY (`documentos_id`) REFERENCES `documentos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ponencias_documentos_ponencias1` FOREIGN KEY (`ponencias_idponencias`) REFERENCES `ponencias` (`idponencias`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for regiones
-- ----------------------------
DROP TABLE IF EXISTS `regiones`;
CREATE TABLE `regiones` (
  `idregiones` int(11) NOT NULL,
  `regionesnombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idregiones`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of regiones
-- ----------------------------
BEGIN;
INSERT INTO `regiones` VALUES (1, 'DEFAULT');
COMMIT;

-- ----------------------------
-- Table structure for reset_password
-- ----------------------------
DROP TABLE IF EXISTS `reset_password`;
CREATE TABLE `reset_password` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `f_registro` datetime NOT NULL,
  `hash_value` varchar(100) NOT NULL,
  `uso` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reset_password_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_reset_password_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rol
-- ----------------------------
DROP TABLE IF EXISTS `rol`;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`usuario_id`),
  KEY `fk_rol_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_rol_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rol
-- ----------------------------
BEGIN;
INSERT INTO `rol` VALUES (1, 1, 'ADMIN', '2021-10-14 14:36:55', '2021-10-14 14:36:58');
COMMIT;

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `bloqueo` tinyint(255) NOT NULL,
  `activo` tinyint(255) NOT NULL,
  `primer_login` tinyint(255) NOT NULL,
  `f_expiracion` date DEFAULT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `status` tinyint(255) DEFAULT '0',
  `ultimo_login` datetime DEFAULT NULL,
  `encuesta_activa` tinyint(1) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `regiones_idregiones` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuario_regiones1_idx` (`regiones_idregiones`),
  CONSTRAINT `fk_usuario_regiones1` FOREIGN KEY (`regiones_idregiones`) REFERENCES `regiones` (`idregiones`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuario
-- ----------------------------
BEGIN;
INSERT INTO `usuario` VALUES (1, 'admin', '$2a$10$uq/1TPTt6inHv.q8kPC6YOTJ/gNVY0NhSyWgbZKwXHDgwfxTlJN0y', 0, 1, 0, '2021-10-14', '2021-10-14 14:31:39', '2021-10-14 14:31:44', 'bmorales.dev@gmail.com', 1, '2021-10-14 14:32:02', 0, NULL, 1);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
