package com.mx.sp.constant;

public class MessagesConstant {

	public static final String DELETE_SUCCESS_MESSAGE ="Se elimin\u00F3 el registro correctamente.";
	
	public static final String EDIT_SUCCESS_MESSAGE ="Se modific\u00F3 el registro correctamente.";
	
	public static final String ADD_SUCCESS_MESSAGE ="Se agreg\u00F3 el registro correctamente.";
	
	public static final String AUTHORIZATION_CHARGE_SUCCESS_MESSAGE ="Se autoriz\u00F3 la carga acad\u00E9mica correctamente.";
	
	public static final String AUTHORIZATION_REMOVE_SUCCESS_MESSAGE = "Se elimin\u00F3 la autorizaci\u00F3n de la carga acad\u00E9mica correctamente.";
	
	public static final String INVALID_FILE_MESSAGE = "Archivo inv\u00E1lido.";
	
	public static final String RESET_PASSWORD_SUCCESS_MESSAGE = "Si los datos son correctos recibir\u00E1s un correo electr\u00F3nico para continuar el proceso.";

	public static final String CHANGE_PASSWORD_SUCCESS_MESSAGE = "Se modific\u00F3 correctamente la contrase\u00F1a.";
	
	public static final String NOT_INIT_PROCESS_MESSAGE = "El proceso aun no ha iniciado.";
	
	public static final String FINISH_PROCESS_MESSAGE = "El proceso ha terminado.";
}
