package com.mx.sp.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.mx.sp.constant.ViewConstant;
import com.mx.sp.repository.UserRepository;
import com.mx.sp.service.impl.UserSecurityService;

/**
 * Controlador principal de la aplicación
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
public class MainController {

	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(MainController.class);
	
	/**
	 * Inyección de UserService
	 */
//	@Autowired
//	private UserService userService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;

	/**
	 * Inyección de UserRepository
	 */
	@Autowired 
	private UserRepository userRepository;
	
	@GetMapping({"/","/home"})
	public String index(Model model, Principal principal) {
		/*UserDto user = userService.findByUsername(principal.getName());
		LOGGER.info("{} - Ingresando a home - USER:{}",  userSecurityService.getIdSession(),user.getUsername());
		if (user.isFirstLogin()) {
			LOGGER.info("{} - Es su primer login - Rediccionando a cambio de contraseña.", userSecurityService.getIdSession());
			model.addAttribute("user", user);
			return ViewConstant.CHANGE_PASSWORD_VIEW;
		} else if(user.isInquest()){
			//Validar si ya existen respuestas guardadas 
			List<AnswersUser> answers = userAnswerService.findByUser(user);
			if(answers.size() > 0) {
				LOGGER.info("{} - Tiene encuesta habilitada, contestada.", userSecurityService.getIdSession());
				user.setInquest(false);
				User userUpdate = userRepository.getOne(user.getIdUser());
				userUpdate.setInquest(false);
				userRepository.save(userUpdate);
				return redirectDashboardByAuthority(user, model, principal);
			}else {
				LOGGER.info("{} - Tiene encuesta habilitada, no contestada.", userSecurityService.getIdSession());
				QuestionDto dto = questionService.find(new QuestionDto());
				dto.setStudentRegister((user.getStudentDto()!=null)?user.getStudentDto().getRegister():(user.getTeacherDto()!=null)?user.getTeacherDto().getRegister():"NA");
				model.addAttribute("questionDto",dto);
				return ViewConstant.INQUEST;
			}
		}else{
			return redirectDashboardByAuthority(user, model, principal);
		}*/
		return ViewConstant.REGISTER;
	}


	
	//@GetMapping("/register")
	public String register() {
		return ViewConstant.REGISTER;
	}

	@GetMapping("/faq")
	public String faq() {
		return ViewConstant.FAQ;
	}

	/*
	@RequestMapping(value = "/profile", method = {GET, POST})
	public String profile(Model model, Principal principal) {
		LOGGER.info("{} - MainController - Inicia carga de perfil.", userSecurityService.getIdSession());
		UserDto userDto = userService.findByUsername(principal.getName());
		userDto.translateAutorities();
		if(userDto.getStudentDto() != null) {
			try {
				if(!userDto.getStudentDto().getName().equals("") && !userDto.getStudentDto().getLastname().equals("") && !userDto.getStudentDto().getSecondLastname().equals("") ) {
					userDto.getStudentDto().setGroupDto(groupService.getGroupById(userDto.getStudentDto().getGroupDto().getId()));
					TutorshipDto tutor = tutorshipService.findByStudent(userDto.getStudentDto());
					userDto.setTutor(tutor.getTeacher());
				}else {
					userDto.setStudentDto(null);
				}
			} catch (ServiceException e) {
				LOGGER.error("Ocurrio un error al cargar el perfil:{}", e.getMessage());
			}
		}
		model.addAttribute("userDto", userDto);
		LOGGER.info("{} - MainController - Termina carga de perfil.", userSecurityService.getIdSession());
		return ViewConstant.PROFILE;
	}
	
	@GetMapping("/changepassword")
	public String userEdit(Model model, Principal principal) {
		LOGGER.info("{} - Inicia cambio de contraseña.", userSecurityService.getIdSession());
		UserDto userDto = userService.findByUsername(principal.getName());
		model.addAttribute("user", userDto);
		LOGGER.info("{} - Termina cambio de contraseña[EXITOSO].", userSecurityService.getIdSession());
		return ViewConstant.CHANGE_PASSWORD_VIEW;
	}
	
	@RequestMapping(value = "/editpassword", method = {GET, POST})
	public String passwordEdit(Model model, Principal principal) {
		LOGGER.info("{} - Inicia editar contraseña, desde el perfil.", userSecurityService.getIdSession());
		UserDto userDto = userService.findByUsername(principal.getName());
		userDto.setHash("EDIT");
		model.addAttribute("user", userDto);
		return ViewConstant.CHANGE_PASSWORD_VIEW ;
	}
	
	@RequestMapping(value="/logoutcontroller",method = RequestMethod.GET)
    public String logout(HttpServletRequest request){
        HttpSession httpSession = request.getSession();
        httpSession.invalidate();
        LOGGER.info("{} - Logout.", userSecurityService.getIdSession());
        return "redirect:/login";
    }*/
}
