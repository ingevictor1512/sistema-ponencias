package com.mx.sp.controller;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;

import com.mx.sp.dto.UserDto;
import com.mx.sp.service.UserService;
import com.mx.sp.service.impl.UserSecurityService;

/**
 * Controller que responde a las peticiones de información de session
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller("sessionController")
public class SessionController {

	/**
	 * Inyeccion de userSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	/**
	 * Inyeccion de userService
	 */
	@Autowired
	private UserService userService;
	
	
	/**
	 * Obtiene el nombre de usuario, listo para presentarlo en el front
	 * @return nombre de usuario
	 */
	public String getUsername() {
		UserDetails usr = userSecurityService.getUserCurrentDetails();
		UserDto userDto = userService.findByUsername(usr.getUsername());
		return userDto.getUsername();
	}
	
	/**
	 * Obtiene la información del ultimo login 
	 * @return ultimo login
	 */
	public String getLastLogin() {
		UserDetails usr = userSecurityService.getUserCurrentDetails();
		UserDto userDto = userService.findByUsername(usr.getUsername());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
		return userDto.getLastLogin()!=null?"\u00DAltimo acceso: "+sdf.format(userDto.getLastLogin()):"";
	}
}
