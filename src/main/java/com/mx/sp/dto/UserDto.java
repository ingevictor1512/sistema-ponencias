package com.mx.sp.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mx.sp.enums.AuthorityEnum;

public class UserDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long idUser;
	private String username;
	private String password;
	private String authority;

	// Aqui van a ir las entidades de Studen y Administrativo

	private Date creationDate;
	private Date editDate;
	private boolean active;
	private boolean firstLogin;
	private Date lastLogin;
	private List<UserDto> users;
	private String verifyPassword;
	private String email;
	private String hash;
	private boolean inquest;
	private String phone;
	private boolean reset;

	public UserDto() {
		this.idUser = -1L;
		this.reset = false;
	}

	public UserDto(long idUser, String username, String password, String authority, Date creationDate, Date editDate,
			boolean active, boolean firstLogin, Date lastLogin, List<UserDto> users, String email, String hash,boolean inquest, String phone) {
		super();
		this.idUser = idUser;
		this.username = username;
		this.password = password;
		this.authority = authority;
		this.creationDate = creationDate;
		this.editDate = editDate;
		this.active = active;
		this.firstLogin = firstLogin;
		this.lastLogin = lastLogin;
		this.users = users;
		this.email = email;
		this.hash = hash;
		this.inquest = inquest;
		this.phone = phone;
	}

	public boolean isInquest() {
		return inquest;
	}

	public void setInquest(boolean inquest) {
		this.inquest = inquest;
	}

	public String getVerifyPassword() {
		return verifyPassword;
	}

	public void setVerifyPassword(String verifyPassword) {
		this.verifyPassword = verifyPassword;
	}

	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(boolean firstLogin) {
		this.firstLogin = firstLogin;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public List<UserDto> getUsers() {
		return users;
	}

	public void setUsers(List<UserDto> users) {
		this.users = users;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean isReset() {
		return reset;
	}

	public void setReset(boolean reset) {
		this.reset = reset;
	}

	public void translateAutorities() {
		switch(authority){
		case "GUEST":
			this.authority = AuthorityEnum.GUEST.getValue();
			break;
		case "ADMIN":
			this.authority = AuthorityEnum.ADMIN.getValue();
			break;
		case "EXPONENT":
			this.authority = AuthorityEnum.EXPONENT.getValue();
			break;
		case "COMPETITOR":
			this.authority = AuthorityEnum.COMPETITOR.getValue();
			break;
		}
	}

	@Override
	public String toString() {
		return "UserDto [idUser=" + idUser + ", username=" + username + ", password=" + password + ", authority="
				+ authority + ", creationDate=" + creationDate + ", editDate=" + editDate + ", active=" + active
				+ ", firstLogin=" + firstLogin + ", lastLogin=" + lastLogin + ", users=" + users + ", verifyPassword="
				+ verifyPassword + ", email=" + email + ", hash=" + hash + ", inquest=" + inquest + ", phone=" + phone + "]";
	}
	
}
