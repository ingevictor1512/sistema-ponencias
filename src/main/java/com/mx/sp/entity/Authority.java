package com.mx.sp.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "rol")
public class Authority implements Serializable, GrantedAuthority{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nombre")
	private String authority;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "usuario_id", nullable=false)
	private User user;
	
	@Column(name = "f_registro")
	@Temporal(TemporalType.TIMESTAMP)
	private Date registerDate;
	
	@Column(name = "f_modif")
	@Temporal(TemporalType.TIMESTAMP)
	private Date editDate;
	
	
	public Authority() {

	}

	public Authority(Long id, String authority, User user, Date registerDate, Date editDate) {
		super();
		this.id = id;
		this.authority = authority;
		this.user = user;
		this.registerDate = registerDate;
		this.editDate = editDate;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	@Override
	public String toString() {
		StringBuilder sbuilder = new StringBuilder();
		sbuilder.append("Authority [id="+id);
		sbuilder.append(", authority="+authority);
//		sbuilder.append(", user="+user);
		sbuilder.append(", registerDate="+registerDate);
		sbuilder.append(", editDate="+editDate);
		sbuilder.append("]");
		return sbuilder.toString();
	}
	
	
	
}