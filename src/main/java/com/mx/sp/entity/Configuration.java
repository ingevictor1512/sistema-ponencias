package com.mx.sp.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Benito Morales Ramirez
 * @version 1.0
 */
@Entity
@Table(name = "config")
public class Configuration implements Serializable {

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "config_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idConfiguration;

	@Column(name = "config_key", nullable = false)
	private String key;

	@Column(name = "config_value", nullable = false)
	private String value;

	@Column(name = "config_desc", nullable = false)
	private String description;
	
	/**
	 * 1:String
	 * 2:Number
	 * 3:Date
	 */
	@Column(name = "config_type", nullable = false)
	private int type;
	
	public Configuration() {
		// Constructor default
	}

	public Configuration(long idConfiguration, String key, String value, String description, int type) {
		super();
		this.idConfiguration = idConfiguration;
		this.key = key;
		this.value = value;
		this.description = description;
		this.type = type;
	}

	public long getIdConfiguration() {
		return idConfiguration;
	}

	public void setIdConfiguration(long idConfiguration) {
		this.idConfiguration = idConfiguration;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Override
	public String toString() {
		StringBuilder sbuilder = new StringBuilder();
		sbuilder.append("Configuration [idConfiguration="+idConfiguration);
		sbuilder.append(", key="+key);
		sbuilder.append(", value="+value);
		sbuilder.append(", description="+description);
		sbuilder.append(", type="+type);
		sbuilder.append("]");
		return sbuilder.toString();
	}

}
