package com.mx.sp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "reset_password")
public class ResetPassword {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idReset;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", nullable=false)
	private User user;
	
	@Column(name = "f_registro")
	private Date created;
	
	@Column(name = "hash_value")
	private String hashValue;
	
	@Column(name = "uso")
	private boolean used;
	
	public long getIdReset() {
		return idReset;
	}

	public void setIdReset(long idReset) {
		this.idReset = idReset;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	public String getHashValue() {
		return hashValue;
	}

	public void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}

	public boolean isUsed() {
		return used;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ResetPassword(int idReset, int idUser, Date created, String hashValue, boolean used) {
		super();
		this.idReset = idReset;
		this.created = created;
		this.hashValue = hashValue;
		this.used = used;
	}

	public ResetPassword() {
		super();
	}

	@Override
	public String toString() {
		StringBuilder sbuilder = new StringBuilder();
		sbuilder.append("ResetPassword [idReset="+idReset);
		sbuilder.append(", created="+created);
		sbuilder.append(", hashValue="+hashValue);
		sbuilder.append(", userId="+user.getIdUser()+", username="+user.getUsername()+"]");
		return sbuilder.toString();
	}
	
}
