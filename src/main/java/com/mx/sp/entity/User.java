 package com.mx.sp.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author Benito Morales
 * @version 1.0
 */
@Entity
@Table(name = "usuario")
public class User implements Serializable {

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1562402792652842098L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idUser;

	@Column
	private String username;

	@Column
	private String password;

	@Column
	private String email;

	@Column(name = "activo")
	private boolean active;

	@Column(name = "bloqueo")
	private boolean locked;

	@Column(name = "status")
	private boolean status;

	@Column(name = "f_registro")
	@Temporal(TemporalType.TIMESTAMP)
	private Date registerDate;

	@Column(name = "f_modif")
	@Temporal(TemporalType.TIMESTAMP)
	private Date editdate;

	@Column(name = "f_expiracion")
	@Temporal(TemporalType.DATE)
	private Date expirationDate;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "user")
	private Set<Authority> authorities;

	@Column(name = "primer_login")
	private boolean firstLogin;
	
	@Column(name = "ultimo_login")
	private Date lastLogin;
	
	@Column(name = "encuesta_activa")
	private boolean inquest;
	
	@Column(name = "telefono")
	private String phone;
	
	public User() {
		authorities = new HashSet<>();
	}

	public User(long idUser, String username, String password, String email, boolean active, boolean locked,
			boolean status, Date registerDate, Date editdate, Date expirationDate, Set<Authority> authorities,
			boolean firstLogin, Date lastLogin, boolean inquest, String phone) {
		super();
		this.idUser = idUser;
		this.username = username;
		this.password = password;
		this.email = email;
		this.active = active;
		this.locked = locked;
		this.status = status;
		this.registerDate = registerDate;
		this.editdate = editdate;
		this.expirationDate = expirationDate;
		this.authorities = authorities;
		this.firstLogin = firstLogin;
		this.lastLogin = lastLogin;
		this.inquest = inquest;
		this.phone = phone;
	}

	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getEditdate() {
		return editdate;
	}

	public void setEditdate(Date editdate) {
		this.editdate = editdate;
	}

	public Date getExpirationdate() {
		return expirationDate;
	}

	public void setExpirationdate(Date expirationdate) {
		this.expirationDate = expirationdate;
	}
	
	public Set<Authority> getAuthorities() {
        return authorities;
    }

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities;
	}
	
	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public boolean isFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(boolean firstLogin) {
		this.firstLogin = firstLogin;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public boolean isInquest() {
		return inquest;
	}

	public void setInquest(boolean inquest) {
		this.inquest = inquest;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		StringBuilder sbuilder = new StringBuilder();
		sbuilder.append("User [idUser="+idUser);
		sbuilder.append(", username="+username);
		sbuilder.append(", password= *******");
		sbuilder.append(", email="+email);
		sbuilder.append(", active="+active);
		sbuilder.append(", locked="+locked);
		sbuilder.append(", status="+status);
		sbuilder.append(", registerDate="+registerDate);
		sbuilder.append(", editdate="+editdate);
		sbuilder.append(", expirationdate="+expirationDate);
		sbuilder.append(", authorities="+authorities.toString());
		sbuilder.append(", inquest="+inquest);
		sbuilder.append(", phone="+phone);
		sbuilder.append("]");
		return sbuilder.toString();
	}

}
