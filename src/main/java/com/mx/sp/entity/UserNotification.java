package com.mx.sp.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.mx.sp.enums.NotificationStatusEnum;
import com.mx.sp.enums.NotificationTypeEnum;

@Entity
@Table(name = "usuario_notificaciones")
public class UserNotification implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idNotificacion;
    
    @Column(name = "nombre_destinatario", nullable = false)
    private String addresseeName;
    
    @Column(name = "email_destinatario", nullable = false)
    private String addresseeMail;
    
    @Column(name = "plain_password", nullable = false)
    private String plainPassword;
    

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usuario_id", nullable = true)
    private User user;
    
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private NotificationStatusEnum status;
    
    @Column(name = "f_expiracion", nullable = true)
    @Temporal(TemporalType.DATE)
    private Date expirationDate;
    
    @Column(name = "tipo_notificacion", nullable = false)
    @Enumerated(EnumType.STRING)
    private NotificationTypeEnum type;
    
    @Column(name = "visto", nullable = false)
    private boolean reviewable;
    
    public UserNotification() {
     // Constructor default
    }
    
    public Long getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(Long idNotificacion) {
        this.idNotificacion = idNotificacion;
    }
    
    public String getAddresseeName() {
        return addresseeName;
    }

    public void setAddresseeName(String addresseeName) {
        this.addresseeName = addresseeName;
    }
    
    public String getAddresseeMail() {
        return addresseeMail;
    }

    public void setAddresseeMail(String addresseeMail) {
        this.addresseeMail = addresseeMail;
    }
    
    public String getPlainPassword() {
        return plainPassword;
    }

    public void setPlainPassword(String passwordApplicant) {
        this.plainPassword = passwordApplicant;
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(User manager) {
        this.user = manager;
    }
    
    public NotificationStatusEnum getStatus() {
        return status;
    }

    public void setStatus(NotificationStatusEnum status) {
        this.status = status;
    }
    
    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }
    
	public NotificationTypeEnum getType() {
		return type;
	}
	
	public void setType(NotificationTypeEnum type) {
		this.type = type;
	}
	
    public boolean isReviewable() {
        return reviewable;
    }

    public void setReviewable(boolean reviewable) {
        this.reviewable = reviewable;
    }

	public UserNotification(Long idNotificacion, String addresseeName, String addresseeMail, String plainPassword,
			User user, NotificationStatusEnum status, Date expirationDate, NotificationTypeEnum type, boolean reviewable) {
		super();
		this.idNotificacion = idNotificacion;
		this.addresseeName = addresseeName;
		this.addresseeMail = addresseeMail;
		this.plainPassword = plainPassword;
		this.user = user;
		this.status = status;
		this.expirationDate = expirationDate;
		this.type = type;
		this.reviewable = reviewable;
	}

	@Override
	public String toString() {
		StringBuilder sbuilder = new StringBuilder();
		sbuilder.append("Notification [idNotificacion="+idNotificacion);
		sbuilder.append(", addresseeName="+addresseeName);
		sbuilder.append(", addresseeMail="+addresseeMail);
		sbuilder.append(", plainPassword="+plainPassword);
		sbuilder.append(", user="+user);
		sbuilder.append(", status="+status);
		sbuilder.append(", expirationDate="+expirationDate);
		sbuilder.append(", type="+type);
		sbuilder.append(", reviewable="+reviewable);
		sbuilder.append("]");
		return sbuilder.toString();
	}
    
}
