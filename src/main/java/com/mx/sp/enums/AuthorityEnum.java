package com.mx.sp.enums;

/**
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
public enum AuthorityEnum {
	GUEST("INVITADO"), ADMIN("ADMINISTRADOR"), EXPONENT("EXPONENTE"), COMPETITOR("PARTICIPANTE");
	
	/**
	 * Valor del enum
	 */
	private String value;
	
	AuthorityEnum(final String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
