package com.mx.sp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mx.sp.entity.ResetPassword;

@Repository("resetPasswordRepository")
public interface ResetPasswordRepository  extends JpaRepository<ResetPassword, Long>{
	@Query("select r from ResetPassword r where r.hashValue =:hash")
	ResetPassword findOneByHashValue(@Param("hash") String hash);
}
