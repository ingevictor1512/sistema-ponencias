package com.mx.sp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.sp.entity.UserNotification;
import com.mx.sp.enums.NotificationStatusEnum;

@Repository("userNotificationRepository")
public interface UserNotificationRepository extends JpaRepository<UserNotification, Long> {

    List<UserNotification> findAllByStatus(NotificationStatusEnum notificationStatus);
    
    UserNotification findByPlainPassword(String plainPassword);
}
