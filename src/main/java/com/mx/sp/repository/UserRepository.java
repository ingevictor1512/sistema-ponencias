package com.mx.sp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.sp.entity.User;

/**
 * UserRepository
 * @author Benito Morales Ramirez
 * @version 1.0
 *
 */
@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long>{

	/**
	 * Obtiene usuario por su username
	 * @param username a buscar
	 * @return Optional user
	 */
	public Optional<User> findByUsername(String username);
	
	/**
	 * Obtiene un usuario por su email
	 * @param email
	 * @return
	 */
	public User findOneByEmailAndUsername(String email, String username);
}
