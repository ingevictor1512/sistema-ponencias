package com.mx.sp.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.mx.sp.converter.UserConverter;
import com.mx.sp.dto.UserDto;
import com.mx.sp.entity.Authority;
import com.mx.sp.entity.Configuration;
import com.mx.sp.entity.ResetPassword;
import com.mx.sp.entity.User;
import com.mx.sp.entity.UserNotification;
import com.mx.sp.enums.ConfigurationEnum;
import com.mx.sp.enums.NotificationStatusEnum;
import com.mx.sp.enums.NotificationTypeEnum;
import com.mx.sp.exception.ServiceException;
import com.mx.sp.repository.AuthorityRepository;
import com.mx.sp.repository.ConfigurationRepository;
import com.mx.sp.repository.ResetPasswordRepository;
import com.mx.sp.repository.UserNotificationRepository;
import com.mx.sp.repository.UserRepository;
import com.mx.sp.service.UserService;
import com.mx.sp.util.SecurityUtils;

/**
 * 
 * @author Benito Morales Ramirez
 * @version 1.0
 *
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	/**
	 * Logger de la aplicacion
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserConverter userConverter;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserNotificationRepository userNotificationRepository;

	@Autowired
	private ResetPasswordRepository resetPasswordRepository;

	@Autowired
	private ConfigurationRepository configurationRepository;

	@Autowired
	private AuthorityRepository authorityRepository;

	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;

	private static final String INVALID_MSG = "La contrase\u00F1a debe tener una longitud de 8 a 20 caracteres, incluir al menos un n\u00FAmero, una letra may\u00FAscula y un caracter especial !@#$%&*-";

	private static final String NOT_MATCH = "Las contrase\u00F1as no coinciden!!!.";

	@Override
	public UserDto findByUsername(String username) {
		User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("No existe usuario"));
		return userConverter.convertUser2UserDto(user);
	}

	@Override
	public UserDto find(UserDto userDto) {
		List<User> users = userRepository.findAll();
		List<UserDto> usersDto = new ArrayList<>();

		for (User user : users) {
			UserDto dto = userConverter.convertUser2UserDto(user);
			dto.translateAutorities();
			usersDto.add(dto);
		}
		userDto.setUsers(usersDto);
		return userDto;
	}

	@Override
	public void save(UserDto userDto) throws ServiceException {
		if (userDto.getIdUser() > -1L) {
			// Update user
			User userUpdate = userConverter.convertUserDto2User(userDto);
			User userPass = userRepository.getOne(userDto.getIdUser());
			Set<Authority> auths = new HashSet<>();
			userPass.getAuthorities().stream().forEach(p -> {
				p.setAuthority(userDto.getAuthority());
				p.setEditDate(new Date());
				auths.add(p);
			});
			userUpdate.setAuthorities(auths);
			userUpdate.setPassword(userPass.getPassword());
			userUpdate.setEditdate(new Date());
			userUpdate.setRegisterDate(userPass.getRegisterDate());
			userRepository.save(userUpdate);
		} else {
			// New user
			User user = userConverter.convertUserDto2User(userDto);
			// user.setAuthorities(authorities);
			user.setActive(true);
			user.setFirstLogin(true);
			user.setRegisterDate(new Date());
			user.setEditdate(new Date());

			if (userDto.getPassword().compareTo(userDto.getVerifyPassword()) != 0) {
				throw new ServiceException(NOT_MATCH);
			}
			if (!SecurityUtils.isValidPassword(userDto.getPassword())) {
				throw new ServiceException(INVALID_MSG);
			}

			// Encrypt
			String encodePass = passwordEncoder.encode(userDto.getPassword());
			user.setPassword(encodePass);
			// Save user
			userRepository.save(user);
		}
	}

	@Override
	public UserDto getById(Long idUser) {
		User user = userRepository.getOne(idUser);
		UserDto userDto = new UserDto();
		userDto.setPassword(null);
		userDto.setVerifyPassword(null);
		return userConverter.convertUser2UserDto(user);
	}

	@Override
	public void delete(Long idUser) throws ServiceException {
		LOGGER.info("Eliminar usuario:{}", idUser);
		userRepository.delete(userRepository.getOne(idUser));
	}

	@Override
	public void updatePassword(UserDto userDto) throws ServiceException {
		LOGGER.info("{} - Inicia cambio de contraseña.", userSecurityService.getIdSession());
		if (!userDto.getPassword().equals(userDto.getVerifyPassword())) {
			throw new ServiceException("Las contrase\u00F1as no coinciden.");
		}

		if (!SecurityUtils.isValidPassword(userDto.getPassword())) {
			throw new ServiceException(INVALID_MSG);
		}

		User user = userRepository.getOne(userDto.getIdUser());
		if (passwordEncoder.matches(userDto.getVerifyPassword(), user.getPassword())) {
			throw new ServiceException("No es posible usar la misma contrase\u00F1a, use una distinta.");
		}

		if (userDto.getHash().equals("EDIT")) {
			if (user != null && user.isActive()) {
				user.setPassword(passwordEncoder.encode(userDto.getPassword()));
				userRepository.save(user);
			} else {
				throw new ServiceException("Error al buscar el usuario.");
			}
			LOGGER.info("{} - Termina edicion de contraseña.", userSecurityService.getIdSession());
		} else {
			if (user != null && user.isActive()) {
				user.setFirstLogin(false);
				user.setPassword(passwordEncoder.encode(userDto.getPassword()));
				userRepository.save(user);

				User userForNotification = userRepository.findOneByEmailAndUsername(user.getEmail(),
						user.getUsername());
				// Guardar notificacion
				UserNotification userNotification = new UserNotification();
				userNotification.setAddresseeMail(user.getEmail());
				userNotification.setAddresseeName("");
				userNotification.setUser(userForNotification);
				userNotification.setPlainPassword("");
				userNotification.setReviewable(false);
				userNotification.setStatus(NotificationStatusEnum.REGISTRADA);
				userNotification.setType(NotificationTypeEnum.CHANGED_P);
				userNotificationRepository.save(userNotification);
				LOGGER.info("{} - Termina cambio de contraseña.", userSecurityService.getIdSession());
			} else {
				throw new ServiceException("Error al buscar el usuario.");
			}
		}
	}

	@Override
	public void resetPasswordForMail(UserDto userDto) throws ServiceException {
		User user = userRepository.findOneByEmailAndUsername(userDto.getEmail(), userDto.getUsername());
		if (user == null) {
			throw new ServiceException("Datos incorrectos!.");
		} else {
			ResetPassword reset = new ResetPassword();
			reset.setUser(user);
			reset.setCreated(new java.util.Date());

			String message = String.format("%s%s", user.getEmail(), new java.util.Date());
			reset.setHashValue(DigestUtils.sha1Hex(message));

			reset.setUsed(false);
			resetPasswordRepository.save(reset);

			UserNotification notification = new UserNotification();
			notification
					.setAddresseeName(user.getUsername());
			notification.setAddresseeMail(user.getEmail());
			notification.setPlainPassword(String.valueOf(reset.getIdReset()));

			User manager = new User();
			manager.setIdUser(user.getIdUser());
			notification.setUser(manager);
			notification.setStatus(NotificationStatusEnum.REGISTRADA);
			notification.setExpirationDate(new java.util.Date());
			notification.setType(NotificationTypeEnum.RESET);
			userNotificationRepository.save(notification);
		}
	}

	@Override
	public void compareHash(String hash) throws ServiceException {
		ResetPassword reset = resetPasswordRepository.findOneByHashValue(hash);
		if (reset == null) {
			throw new ServiceException("Token inv\u00E1lido.");
		}
		// Validar si el token no ha caducado
		Configuration expirationDay = configurationRepository.findByKey(ConfigurationEnum.EXPIRATION_DAY.toString());

		Date current = new Date();
		Calendar initialCalendar = Calendar.getInstance();
		initialCalendar.setTime(reset.getCreated());
		initialCalendar.add(Calendar.DATE, Integer.valueOf(expirationDay.getValue()));

		if (current.after(initialCalendar.getTime())) {
			throw new ServiceException("Este token ya no es v\u00E1lido.");
		}

		if (reset.isUsed()) {
			throw new ServiceException("Este token ya ha sido utilizado.");
		}
	}

	@Override
	public void updatePasswordForReset(UserDto form) throws ServiceException {

		if (form.getPassword().compareTo(form.getVerifyPassword()) != 0) {
			throw new ServiceException(NOT_MATCH);
		}
		if (!SecurityUtils.isValidPassword(form.getPassword())) {
			throw new ServiceException(INVALID_MSG);
		}
		
		ResetPassword reset = null;
		User user = null;
		try {
			reset = resetPasswordRepository.findOneByHashValue(form.getHash());
			LOGGER.info("ResetObject.id:{}", reset.getIdReset());
			// Ocasionalmente manda nullPointer en producción
			user = userRepository.getOne(Long.valueOf(String.valueOf(reset.getUser().getIdUser())));
		} catch (Exception ex) {
			LOGGER.error("Error al resetear contraseña por hash:[{}][{}]", form.getHash(), ex.getMessage());
			throw new ServiceException(
					"Ocurri\u00F3 un problema al procesar tu solicitud, favor de intentarlo de nuevo.");
		}
		reset.setUsed(true);
		user.setPassword(passwordEncoder.encode(form.getPassword()));
		resetPasswordRepository.save(reset);
		userRepository.save(user);
	}
}
