/**
 * 
 */

var SAUA = {};

SAUA.TIMEOUT = 3000;

SAUA.tutor = function(idInput, idValue) {
	$("#" + idInput, "#tutor-form").val(idValue);
	$("#tutor-form").submit();
};
SAUA.edit = function(idInput, idValue) {
	$("#" + idInput, "#edit-form").val(idValue);
	$("#edit-form").submit();
};

SAUA.addCharge = function(idInput, idValue) {
	$("#" + idInput, "#charge-form").val(idValue);
	$("#charge-form").submit();
};

SAUA.resetPassword = function(idInput, idValue) {
	$("#" + idInput, "#reset-form").val(idValue);
	$("#reset-form").submit();
};

SAUA.show = function(idInput, idValue) {
	$("#" + idInput, "#show-form").val(idValue);
	$("#show-form").submit();
};

SAUA.authorized = function(idInput, idValue) {
	$("#" + idInput, "#authorized-form").val(idValue);
	$("#authorized-form").submit();
};
SAUA.kardex = function(idInput, idValue) {
	$("#" + idInput, "#kardex-form").val(idValue);
	$("#kardex-form").submit();
};


SAUA.validatePassword = function() {
	var passwTyped = $("#password_").val();
	if(passwTyped.match("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*-]).{8,20})"))
		alert("La matrícula es correcta"); 
	else 
		alert("La matrícula NO es correcta");
	
};

SAUA.toDelete = function(idInput, idValue) {
	$("#" + idInput, "#delete-form").val(idValue);
	swal({
        title: 'Eliminar registro',
        text: "Está seguro de eliminar el registro?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        confirmButtonText: 'Great ',
        buttons: {
          cancel: {
            text: "Cancelar",
            value: null,
            visible: true,
            className: "btn btn-danger",
            closeModal: true,
          },
          confirm: {
            text: "Aceptar",
            value: true,
            visible: true,
            className: "btn btn-primary",
            closeModal: true
          }
        }
      }).then(function (result) {
    	  if(result){
    		  $("#delete-form").submit();
    	  }
      });
};

SAUA.toAuthorization = function(idInput, idValue) {
	$("#" + idInput, "#authorization-form").val(idValue);
	swal({
        title: 'Autorizar carga acad\u00E9mica',
        text: "Al autorizar la carga del alumno ya no se podrán hacer modificaciones",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        confirmButtonText: 'Great ',
        buttons: {
          cancel: {
            text: "Cancelar",
            value: null,
            visible: true,
            className: "btn btn-danger",
            closeModal: true,
          },
          confirm: {
            text: "Continuar",
            value: true,
            visible: true,
            className: "btn btn-primary",
            closeModal: true
          }
        }
      }).then(function (result) {
    	  if(result){
    		  $("#authorization-form").submit();
    	  }
      });
};
SAUA.toDesAuthorization = function(idInput, idValue) {
	$("#" + idInput, "#desauthorized-form").val(idValue);
	swal({
        title: 'Remover autorización de carga acad\u00E9mica',
        text: "Al quitar la autorización, el alumno podrá hacer modificaciones (eliminar o agregar uaps)",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        confirmButtonText: 'Great ',
        buttons: {
          cancel: {
            text: "Cancelar",
            value: null,
            visible: true,
            className: "btn btn-danger",
            closeModal: true,
          },
          confirm: {
            text: "Continuar",
            value: true,
            visible: true,
            className: "btn btn-primary",
            closeModal: true
          }
        }
      }).then(function (result) {
    	  if(result){
    		  $("#desauthorized-form").submit();
    	  }
      });
};

SAUA.selectProgramOnOfferedClass = function(){
	$.ajax({
		url : '/api/uaps/findbyprogram/'+$("#program option:selected").val(),
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success :function(data){
			  $("#uap").empty();
			  $("#uap").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#uap").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
	$.ajax({
		url : '/api/groups/find/'+$("#program option:selected").val(),
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#group").empty();
			  $("#group").append('<option value="-1">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#group").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.chargePrograms =  function(){
	$.ajax({
		url : '/api/programs/find/',
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#program").empty();
			  $("#program").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#program").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.chargeTeachers =  function(){
	$.ajax({
		url : '/api/teachers/find/',
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#teacher").empty();
			  $("#teacher").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#teacher").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.chargeTeachersModal2 =  function(){
	$.ajax({
		url : '/api/teachers/find/',
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#teacheruno").empty();
			  $("#teacheruno").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#teacheruno").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.selectProgramOnChargeStudent =  function(){
	$.ajax({
		url : '/api/groups/find/'+$("#program option:selected").val(),
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#group").empty();
			  $("#group").append('<option value="">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#group").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.uapsForGroup =  function(){
	$.ajax({
		url : '/api/groups/offeredclass/find/'+$("#group option:selected").val(),
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#uaps").empty();
			  $("#uaps").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#uaps").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.saveCharge = function(){
	 var incomplete = false;
	  if ($("#program option:selected").val() == 'SN' || $("#group option:selected").val() == '-1' 
			  || $("#uaps option:selected").val() == 'SN' || $("#studyTypes option:selected").val() == '') {
	    incomplete = true;
	  }
	   
	  if (incomplete) {
		  swal({
		        title: 'Informac\u00F3in incompleta',
		        text: "Faltan campos importantes por seleccionar.",
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3f51b5',
		        cancelButtonColor: '#ff4081',
		        confirmButtonText: 'Great ',
		        buttons: {
		          cancel: {
		            text: "Aceptar",
		            value: null,
		            visible: true,
		            className: "btn btn-danger",
		            closeModal: true,
		          }
		      }
		  })
	      return;
	  }else {	 
			$.ajax({
				  headers:
			        { 'X-CSRF-TOKEN': $('#csrf').val(),
			          'Accept': 'application/json',
			          'Content-Type': 'application/json'                    
			        },
					url : '/api/students/savecharge',
					method : 'post',
					cache : false,
					contentType : 'application/json',
					dataType : 'json',
					data : JSON.stringify({
						"idProgram" : $("#program option:selected").val(),
						"idGroup" : $("#group option:selected").val(),
						"idUap" : $("#uaps option:selected").val(),
						"studyType" : $("#studyTypes option:selected").val(),
						"studentRegister" : $("#studentRegister").val()
					}),
					success : function(data){
			        	console.log("Success!!!!");
			        	$("#exampleModal-4").modal('hide');
			        	swal({
			                title: '',
			                text: 'Clase agregada correctamente!!',
			                icon: 'success',
			                button: {
			                  text: "Ok",
			                  value: true,
			                  visible: true,
			                  className: "btn btn-primary"
			                }
			              }).then(function() {
			            	    window.location.reload();
			              });
					},
					error: function(data) {
						console.log("Error!!!!");
						swal({
					        title: '',
					        text: data.responseText,
					        icon: 'error',
					        button: {
					          text: "Ok",
					          value: true,
					          visible: true,
					          className: "btn btn-warning"
					        }
					      });
						$("#exampleModal-4").modal('hide');
					}
				});
	  }
}
SAUA.saveTutortoGroup = function(){
	 var incomplete = false;
	  if ($("#program option:selected").val() == 'SN' || $("#group option:selected").val() == '-1' 
			  || $("#teacher option:selected").val() == 'SN') {
	    incomplete = true;
	  }
	   
	  if (incomplete) {
		  swal({
		        title: 'Informaci\u00F3n incompleta',
		        text: "Faltan campos importantes por seleccionar.",
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3f51b5',
		        cancelButtonColor: '#ff4081',
		        confirmButtonText: 'Great ',
		        buttons: {
		          cancel: {
		            text: "Aceptar",
		            value: null,
		            visible: true,
		            className: "btn btn-danger",
		            closeModal: true,
		          }
		      }
		  })
	      return;
	  }else {	 
			$.ajax({
				  headers:
			        { 'X-CSRF-TOKEN': $('#csrf').val(),
			          'Accept': 'application/json',
			          'Content-Type': 'application/json'                    
			        },
					url : '/api/tutorship/saveTutortoGroup',
					method : 'post',
					cache : false,
					contentType : 'application/json',
					dataType : 'json',
					data : JSON.stringify({
						"idProgram" : $("#program option:selected").val(),
						"idGroup" : $("#group option:selected").val(),
						"idTeacher" : $("#teacher option:selected").val()
					}),
					success : function(data){
			        	console.log("Success!!!!");
			        	$("#exampleModal-4").modal('hide');
			        	swal({
			                title: '',
			                text: 'Tutor asignado correctamente!!',
			                icon: 'success',
			                button: {
			                  text: "Ok",
			                  value: true,
			                  visible: true,
			                  className: "btn btn-primary"
			                }
			              }).then(function() {
			            	    window.location.reload();
			              });
					},
					error: function(data) {
						console.log("Error!!!!");
						swal({
					        title: 'No se puede asignar el tutor al grupo',
					        text: data.responseText,
					        icon: 'error',
					        button: {
					          text: "Ok",
					          value: true,
					          visible: true,
					          className: "btn btn-warning"
					        }
					      });
						$("#exampleModal-4").modal('hide');
					}
				});
	  }
}
SAUA.saveStudentsToTutor = function(){
	 var incomplete = false;
	  if ($("#teacheruno option:selected").val() == '-1' ) {
	    incomplete = true;
	  }
	   
	  if (incomplete) {
		  swal({
		        title: 'Informaci\u00F3n incompleta',
		        text: "Faltan campos importantes por seleccionar.",
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3f51b5',
		        cancelButtonColor: '#ff4081',
		        confirmButtonText: 'Great ',
		        buttons: {
		          cancel: {
		            text: "Aceptar",
		            value: null,
		            visible: true,
		            className: "btn btn-danger",
		            closeModal: true,
		          }
		      }
		  })
	      return;
	  }else {	 
			$.ajax({
				  headers:
			        { 'X-CSRF-TOKEN': $('#csrf').val(),
			          'Accept': 'application/json',
			          'Content-Type': 'application/json'                    
			        },
					url : '/api/tutorship/saveStudentsToTutor',
					method : 'post',
					cache : false,
					contentType : 'application/json',
					dataType : 'json',
					data : JSON.stringify({
						"register" : $("#matriculas").val(),
						"idTeacher" : $("#teacheruno option:selected").val()
					}),
					success : function(data){
			        	console.log("Success!!!!");
			        	$("#exampleModal-4").modal('hide');
			        	swal({
			                title: '',
			                text: 'Tutor asignado correctamente!!',
			                icon: 'success',
			                button: {
			                  text: "Ok",
			                  value: true,
			                  visible: true,
			                  className: "btn btn-primary"
			                }
			              }).then(function() {
			            	    window.location.reload();
			              });
					},
					error: function(data) {
						console.log("Error!!!!");
						swal({
					        title: 'No se pueden asignar los alumnos al tutor',
					        text: data.responseText,
					        icon: 'error',
					        button: {
					          text: "Ok",
					          value: true,
					          visible: true,
					          className: "btn btn-warning"
					        }
					      });
						$("#exampleModal-4").modal('hide');
					}
				});
	  }
}
SAUA.getCharge = function(){
	$.ajax({
		  headers:
	        { 'X-CSRF-TOKEN': $('#csrf').val(),
	          'Accept': 'application/json',
	          'Content-Type': 'application/json'                    
	        },
			url : '/api/students/getcharge/'+$("#studentRegister").val(),
			method : 'get',
			cache : false,
			contentType : 'application/json',
			dataType : 'json',
			success : function(data){
	        	$('#order-listing tbody tr').slice(0).remove();
	        	$.each(data,function(key,registro) {
		        	$("#order-listing").append("<tr>"
		        			+"<td>"+registro[0]+"</td>"
		        			+"<td>"+registro[1]+"</td>"
		        			+"<td>"+registro[2]+"</td>"
		        			+"<td>"+registro[3]+"</td>"
		        			+"<td>"+registro[4]+"</td>"
		        			+"<td>"+registro[5]+"</td>"
		        			+"<td>"+registro[6]+"</td>"
		        			+"<td>"
		        			+ "<button type='button' class='btn btn-outline-danger' data-toggle='modal' "
		        					+ "title='Borrar' onclick='SAUA.toDelete(\"id\", "+registro[0]+");'>"
		        					+ "<span class='icon-trash'></span> "
		        			+ "</button>"
		        		+"</td>");
			     });
	        	//window.location.reload();
			},
			error: function(data) {
				console.log("Error al cargar los datos");
			}
		});
}

SAUA.saveQuestions = function(){
if($("#questions-form").valid()) {
	var list = new Array();
	$("input[type=text]").each(function(){
		if(this.value == "") {
			list.push(this.id+"-empty");
		}else {
			list.push(this.id+"-"+this.value);
		}
	});
	$("input[type=radio]:checked").each(function(){
		list.push(this.id+"-"+this.value);
	});

	var check = 0;
	$("input[type=checkbox]:checked").each(function(){
		list.push(this.id+"-"+this.value);
		check++
	});
	
	if(check > 0){
	$.ajax({
		  headers:
	        { 'X-CSRF-TOKEN': $('#csrf').val(),
	          'Accept': 'application/json',
	          'Content-Type': 'application/json'                    
	        },
			url : '/api/inquest/save',
			method : 'post',
			cache : false,
			contentType : 'application/json',
			dataType : 'json',
			data : JSON.stringify({
				"studentRegister" : $("#register").val(),
				"answersl" : list
			}),
			success : function(data){
	        	console.log("Success!!!!");
	        	swal({
	                title: 'Informaci\u00F3n guardada correctamente.',
	                text: 'Gracias por tu participaci\u00F3n',
	                icon: 'success',
	                button: {
	                  text: "Ok",
	                  value: true,
	                  visible: true,
	                  className: "btn btn-primary"
	                }
	              }).then(function() {
	            	    window.location.reload();
	              });
			},
			error: function(data) {
				console.log("Error!!!!");
				swal({
			        title: '',
			        text: data.responseText,
			        icon: 'error',
			        button: {
			          text: "Ok",
			          value: true,
			          visible: true,
			          className: "btn btn-warning"
			        }
			      });
			}
		});
		}else{
			swal({
		        title: 'Faltan opciones por seleccionar',
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}
}

SAUA.changePeriod = function(idInput, idValue){
	$("#" + idInput, "#active-form").val(idValue);
	swal({
        title: 'Activar periodo',
        text: "Al activar este periodo, el que est\u00E9 activo, se desactivar\u00E1. Esta seguro de activarlo?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        confirmButtonText: 'Great ',
        buttons: {
          cancel: {
            text: "Cancelar",
            value: null,
            visible: true,
            className: "btn btn-danger",
            closeModal: true,
          },
          confirm: {
            text: "Aceptar",
            value: true,
            visible: true,
            className: "btn btn-primary",
            closeModal: true
          }
        }
      }).then(function (result) {
    	  if(result){
    		  $("#active-form").submit();
    	  }
      });
}

SAUA.groupAssign = function(idInput, idValue, idInputTwo, idProgram) {
	$.ajax({
		url : '/api/groups/find/'+idProgram,
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#groups-req").empty();
			  $("#groups-req").append('<option value="-1">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#groups-req").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
	
	$("#exampleModal-4").modal("show");
	$("#student-register").val(idValue);
	$("#studentRegister").val(idValue);
};

SAUA.uapsRequiredForGroup =  function(){
	$('#order-listing-two tbody tr').slice(0).remove();
	$.ajax({
		url : '/api/groups/offeredclassrequired/find/'+$("#groups-req option:selected").val(),
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			//$('#required-uaps').show();
        	$.each(data,function(key,registro) {
	        	$("#order-listing-two").append("<tr>"
	        			+"<td>"+registro[0]+"</td>"
	        			+"<td>"+registro[1]+"</td>");
		     });
		},
		error: function(data) {
			console.log("Error al cargar los datos");
		}
	});
}

SAUA.saveaddtogroup = function(){
	 var incomplete = false;
	  if ($("#groups-req option:selected").val() == '-1') {
	    incomplete = true;
	  }
	   
	  if (incomplete) {
		  swal({
		        title: 'Informac\u00F3in incompleta',
		        text: "Falta seleccionar el grupo.",
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3f51b5',
		        cancelButtonColor: '#ff4081',
		        confirmButtonText: 'Great ',
		        buttons: {
		          cancel: {
		            text: "Aceptar",
		            value: null,
		            visible: true,
		            className: "btn btn-danger",
		            closeModal: true,
		          }
		      }
		  })
	      return;
	  }else {
	  		console.log("GROUP_ID:"+$("#group option:selected").val());
			$.ajax({
				  headers:
			        { 'X-CSRF-TOKEN': $('#csrf').val(),
			          'Accept': 'application/json',
			          'Content-Type': 'application/json'                    
			        },
					url : '/api/students/addtogroup',
					method : 'post',
					cache : false,
					contentType : 'application/json',
					dataType : 'json',
					data : JSON.stringify({
						"idGroup" : $("#group option:selected").val(),
						"studentRegister" : $("#studentRegister").val()
					}),
					success : function(data){
			        	console.log("Success!!!!");
			        	$("#exampleModal-4").modal('hide');
			        	swal({
			                title: '',
			                text: 'Alumno agregado correctamente!!',
			                icon: 'success',
			                button: {
			                  text: "Ok",
			                  value: true,
			                  visible: true,
			                  className: "btn btn-primary"
			                }
			              }).then(function() {
			            	    window.location.reload();
			              });
					},
					error: function(data) {
						console.log("Error!!!!");
						swal({
					        title: '',
					        text: data.responseText,
					        icon: 'error',
					        button: {
					          text: "Ok",
					          value: true,
					          visible: true,
					          className: "btn btn-warning"
					        }
					      });
						$("#exampleModal-4").modal('hide');
					}
				});
	  }
}


SAUA.groupList = function(idInput, idValue, name, description) {
	$.ajax({
		url : '/api/uaps/findbygroup/'+idValue,
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#uaps").empty();
			  $("#uaps").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#uaps").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
	
	$("#exampleModal-4").modal("show");
	$("#idGroup").val(idValue);
	$("#id-group").val(description);
};

SAUA.grouptopromote = function(idInput, idValue) {
	$.ajax({
		url : '/api/groups/findbypromote/'+idValue,
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#groups").empty();
			  $("#groups").append('<option value="SN">Seleccione</option>');
			  if(data.length == 0){
				swal({
				        title: 'Siguiente grupo no encontrado.',
				        text: "El siguiente grupo relacionado a este, aun no esta dado de alta.",
				        icon: 'warning',
				        showCancelButton: true,
				        confirmButtonColor: '#3f51b5',
				        cancelButtonColor: '#ff4081',
				        confirmButtonText: 'Great ',
				        buttons: {
				          cancel: {
				            text: "Aceptar",
				            value: null,
				            visible: true,
				            className: "btn btn-danger",
				            closeModal: true,
				          }
				      }
				  }).then(function (result) {
			    	  $("#exampleModal-4").modal("hide");
			    	  return;
			      });
			  }
		      $.each(data,function(key,registro) {
			        $("#groups").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
	
	$("#exampleModal-4").modal("show");
	$("#idGroup").val(idValue);
};

SAUA.promote = function(){
	var incomplete = false;
	 if ($("#groups option:selected").val() == 'SN') {
	   incomplete = true;
	 }
	 
 	if (incomplete) {
	  swal({
	        title: 'Informac\u00F3in incompleta',
	        text: "Debe seleccionar un grupo.",
	        icon: 'warning',
	        showCancelButton: true,
	        confirmButtonColor: '#3f51b5',
	        cancelButtonColor: '#ff4081',
	        confirmButtonText: 'Great ',
	        buttons: {
	          cancel: {
	            text: "Aceptar",
	            value: null,
	            visible: true,
	            className: "btn btn-danger",
	            closeModal: true,
	          }
	      }
	  })
      return;
  	}else {	
		$.ajax({
		  headers:
	        { 'X-CSRF-TOKEN': $('#csrf').val(),
	          'Accept': 'application/json',
	          'Content-Type': 'application/json'                    
	        },
			url : '/api/groups/promote',
			method : 'post',
			cache : false,
			contentType : 'application/json',
			dataType : 'json',
			data : JSON.stringify({
				"idGroup" : $("#idGroup").val(),
				"idGroupPromote" : $("#groups option:selected").val()
			}),
			success : function(data){
	        	console.log("Success!!!!");
	        	$("#exampleModal-4").modal('hide');
	        	swal({
	                title: '',
	                text: 'Alumnos promovidos correctamente!!',
	                icon: 'success',
	                button: {
	                  text: "Ok",
	                  value: true,
	                  visible: true,
	                  className: "btn btn-primary"
	                }
	              }).then(function() {
	            	    window.location.reload();
	              });
			},
			error: function(data) {
				console.log("Error!!!!");
				swal({
			        title: '',
			        text: data.responseText,
			        icon: 'error',
			        button: {
			          text: "Ok",
			          value: true,
			          visible: true,
			          className: "btn btn-warning"
			        }
			      });
				$("#exampleModal-4").modal('hide');
			}
		});
	}
}

SAUA.printList = function(){
	var incomplete = false;
	  if ($("#uaps option:selected").val() == 'SN') {
	    incomplete = true;
	  }
	
	if (incomplete) {
		  swal({
		        title: 'Informac\u00F3in incompleta',
		        text: "Debe seleccionar una UAP.",
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3f51b5',
		        cancelButtonColor: '#ff4081',
		        confirmButtonText: 'Great ',
		        buttons: {
		          cancel: {
		            text: "Aceptar",
		            value: null,
		            visible: true,
		            className: "btn btn-danger",
		            closeModal: true,
		          }
		      }
		  })
	      return;
	  }else {	
		$.ajax({
		  headers:
	        { 'X-CSRF-TOKEN': $('#csrf').val(),
	          'Accept': 'application/json',
	          'Content-Type': 'application/json'                    
	        },
			url : '/api/lists/studentsbyuap',
			method : 'post',
			cache : false,
			data : JSON.stringify({
				"idGroup" : $("#idGroup").val(),
				"idUap" : $("#uaps option:selected").val(),
				"filename" : $("#id-group").val() +' - '+ $("#uaps option:selected").text()
			}),
			xhrFields: {
	            responseType: 'blob'
	        },
	        success: function (data, status, response) {
	            var filename = "";
	            var disposition = response.getResponseHeader('Content-Disposition');
	            if (disposition && disposition.indexOf('attachment') !== -1) {
	                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
	                var matches = filenameRegex.exec(disposition);
	                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
	            }
	            let a = document.createElement('a');
	            a.href = window.URL.createObjectURL(data);
	            a.download = filename;
	            document.body.append(a);
	            a.click();
				data = null;
	        },
			error: function(data) {
				console.log("Error!!!!");
				swal({
			        title: '',
			        text: 'Ocurri\u00F3 un error al generar lista.',
			        icon: 'error',
			        button: {
			          text: "Ok",
			          value: true,
			          visible: true,
			          className: "btn btn-warning"
			        }
			      });
				$("#exampleModal-4").modal('hide'); 
			}
		}).done(function() {
		  console.log("Success!!!!");
	    	$("#exampleModal-4").modal('hide');
	    	swal({
	            title: '',
	            text: 'Lista generada correctamente!!',
	            icon: 'success',
	            button: {
	              text: "Ok",
	              value: true,
	              visible: true,
	              className: "btn btn-primary"
	            }
	          });
			
		});
	}
}

SAUA.documentdownload = function(idInput, idValue){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/download/document/'+idValue,
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			swal({
		        title: '',
		        text: 'No se pudo descargar archivo.',
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	});
}

SAUA.papeletadownload = function(){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/download/papeleta',
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			swal({
		        title: '',
		        text: 'No se pudo descargar papeleta.',
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	});
}

SAUA.papeletadownloadByStudent = function(idInput, idValue){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/download/papeleta/'+idValue,
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			var errorType = "No se pudo descargar papeleta.";
			if(data.status == '501'){
				errorType = "El usuario no es un alumno.";
			}
			if(data.status == '502'){
				errorType = "El usuario no esta autorizado para imprimir papeleta.";
			}
			if(data.status == '503'){
				errorType = "Carga acad\u00E9mica no autorizada.";
			}
			swal({
		        title: '',
		        text: errorType,
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	});
}

SAUA.papeletadownloadMasMat = function(idInput, idValue){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/download/papeletasMasMat/'+idValue,
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			var errorType = "No se pudo descargar papeleta.";
			if(data.status == '501'){
				errorType = "El usuario no es un alumno.";
			}
			if(data.status == '502'){
				errorType = "El usuario no esta autorizado para imprimir papeleta.";
			}
			if(data.status == '503'){
				errorType = "Carga acad\u00E9mica no autorizada.";
			}
			swal({
		        title: '',
		        text: errorType,
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	});
}

SAUA.uapsAssingToStudent = function(idInput, idValue){

			$.ajax({
				  headers:
			        { 'X-CSRF-TOKEN': $('#csrf').val(),
			          'Accept': 'application/json',
			          'Content-Type': 'application/json'                    
			        },
					url : '/api/programs/uapsassign/'+idValue,
					method : 'get',
					cache : false,
					contentType : 'application/json',
					dataType : 'json',
					success : function(data){
			        	console.log("Success!!!!");
			        	swal({
			                title: 'Asignación realizada correctamente',
			                text: 'Uaps asignadas correctamente a todos los grupos del programa educativo seleccionado!!',
			                icon: 'success',
			                button: {
			                  text: "Ok",
			                  value: true,
			                  visible: true,
			                  className: "btn btn-primary"
			                }
			              }).then(function() {
			            	    window.location.reload();
			              });
					},
					error: function(data) {
						console.log("Error!!!!");
						swal({
					        title: 'No se realizó la asignación de uaps a los grupos',
					        text: data.responseText,
					        icon: 'error',
					        button: {
					          text: "Ok",
					          value: true,
					          visible: true,
					          className: "btn btn-warning"
					        }
					      });
						
					}
				});
	  
}

SAUA.statisticsUAPs = function(){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/download/statistics/uaps',
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			var errorType = "No se pudo generar el reporte.";
			if(data.status == '502'){
				errorType = "El usuario no esta autorizado para imprimir este reporte.";
			}
			swal({
		        title: '',
		        text: errorType,
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	});
}